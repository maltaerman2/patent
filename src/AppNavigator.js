import { createStackNavigator } from 'react-navigation';
import Home from './Home';

const AppNavigator = createStackNavigator({
  Home: { screen: Home },
  Camera: { screen: Camera },
});

export default AppNavigator;